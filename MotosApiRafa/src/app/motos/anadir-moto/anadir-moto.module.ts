import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AnadirMotoPageRoutingModule } from './anadir-moto-routing.module';
import { AnadirMotoPage } from './anadir-moto.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AnadirMotoPageRoutingModule,
    ComponentsModule
  ],
  declarations: [AnadirMotoPage]
})
export class AnadirMotoPageModule {}
