import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatosService } from 'src/app/service/datos.service';

@Component({
  selector: 'app-anadir-moto',
  templateUrl: './anadir-moto.page.html',
  styleUrls: ['./anadir-moto.page.scss'],
})
export class AnadirMotoPage implements OnInit {

  marcaa: string = '';
  modeloo: string = '';
  year: string = '';
  precioo: string = '';
  photo: string = '';
  cameraImage: string | ArrayBuffer = '../../../assets/camera.svg';

  constructor(private data: DatosService, private router: Router) { }

  ngOnInit() {
  }

  cameraImageChange(file: File){

    this.photo = file[0];
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    reader.onload = () => {
      this.cameraImage = reader.result;
    };
  }

  anadirNuevaMoto(){

    const formData = new FormData();
    const priceAsString = this.precioo + '';

    formData.append('foto', this.photo);
    formData.append('marca', this.marcaa);
    formData.append('modelo', this.modeloo);
    formData.append('year', this.year);
    formData.append('precio', priceAsString);

    this.data.anadirMoto(formData);
    this.router.navigate(['/motos']);
  
  }
}
