import { Injectable } from '@angular/core';
import { Moto } from '../motos/motos.model';

@Injectable({
  providedIn: 'root'
})

export class DatosService {

  BASE_URL: string = "`http://motos.puigverd.org`"; //api Rafa
  motos: Moto[] = [];
  singleMoto: Moto;

  constructor() { }

  async getMotos(){

    this.motos = [];
    this.motos = await (await fetch(`${this.BASE_URL}/motos`)).json();
  
    return this.motos;

  }

  setSingleMoto(motoSeleccionada: Moto){

    this.singleMoto = motoSeleccionada;

  }

  getSingleMoto(){

    return this.singleMoto;

  }

  async getMotoFiltrada(marcaMoto: string){

    this.motos = await (await fetch(`${this.BASE_URL}/motos?marca=${marcaMoto}`)).json();

    return this.motos;

  }

  async borrarMoto(id: number){

    const eliminarMoto = await fetch(`${this.BASE_URL}/moto/${id}`, {method: 'DELETE',});

    return eliminarMoto;

  }

  async anadirMoto(nuevaMoto){

    const nuevaMotoAlbert = await fetch(`${this.BASE_URL}/moto/foto`, {method: 'POST', body: nuevaMoto,});

  }
}
