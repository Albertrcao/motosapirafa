import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MotosInfoComponent } from './motos-info/motos-info.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    MotosInfoComponent,
    HeaderComponent
  ],

  imports: [
    CommonModule,
    IonicModule,
  ],

  exports: [
    MotosInfoComponent, 
    HeaderComponent
  ]
  
})
export class ComponentsModule { }
